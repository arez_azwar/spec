# Issue reporter

1. [Bubble UI](#bubble-ui)
1. [Issue Reporter Tab](#issue-reporter-tab)
    1. [Issue Reporter Entry](#issue-reporter-entry)
    1. [Issue Reporter Comment](#issue-reporter-comment)
    1. [Report Data Overlay](#report-data-overlay)
    1. [Send Report](#send-report)
    1. [Send Report Sent](#send-report-sent)
1. [Data gathering tab](#data-gathering-tab)
1. [Report Data](#report-data)
1. [Assets](#assets)

## Bubble UI

[Bubble UI Design](https://eyeogmbh.invisionapp.com/share/3ESHOTLH9UD#/screens/368547227)

Clicking `Report issue` in the Bubble UI will:

1. Open the [Issue Reporter Tab](#issue-reporter-tab) in a new active tab.
1. Open the page the user had opened in the previously active tab in the [Data gathering tab](#data-gathering-tab). The [Data gathering tab](#data-gathering-tab) should be opened in a new background tab to record all [Requests](#requests) and [Filters](#filters) to include in the [Report Data](#report-data).
1. Display a message within a banner on the Issue reporter (on all steps) `**Note**: An additional tab will temporarily open so the page you are on won't be affected by the Issue Reporter.`, the banner should be closeable via the *X* icon.

![](/res/abp/issue-reporter/select-issue.jpg)

[Back to top of page](#issue-reporter)

## Issue reporter tab

### Issue Reporter Navigation

![](/res/abp/issue-reporter/navigation.jpg)

| # | Text |
| --- | --- |
| 0.1 | `Adblock **Plus**` |
| 0.2 | `Issue reporter` |
| 0.3 | `Select issue` |
| 0.4 | `Mark issue` |
| 0.5 | `Details` |
| 0.6 | `Done` |

Step progress bar indicates to the user where they are in the issue reporter flow. Each step within the progress bar will not be clickable until after the user reaches that step, then it becomes clickable. 

i.e. If a user is still on step 2, he/she will not be able to click to step 4, but will be able to click to step 1. 

**Active - Inactive - Incomplete - Incomplete**

![](/res/abp/issue-reporter/progress-bar-1.jpg)

**Complete - Active - Incomplete - Incomplete**

![](/res/abp/issue-reporter/progress-bar-2.jpg)

#### Navigation assets

| Item | File | alt text |
| --- | --- | --- |
| abp-logo.svg | ![](/res/abp/issue-reporter/assets/abp-logo.svg) | `Adblock Plus logo` |
| checkmark.svg | ![](/res/abp/issue-reporter/assets/checkmark.svg) | `checkmark` |

### Issue Type

![](/res/abp/issue-reporter/issue-type.jpg)

| # | Text |
| --- | --- |
| 1.0 | [Screenshot accessibility warning](#screenshot-accessibility-warning) |
| 1.1 | `What type of issue are you experiencing?` |
| 1.2 | `Please select an issue:` |
| 1.3 | `I still see ads` |
| 1.4 | `Adblock Plus is enabled, but I still see an ad or ads.` |
| 1.5 | `The page I'm trying to view is broken` |
| 1.6 | `The page displays incorrectly, fails to function or seems to be missing important content.` |
| 1.7 | [`Cancel` Button](#cancel-button) |
| 1.8 | [`Continue` Button](#continue-button) |
| 1.9 | `Privacy policy` opens [Documentation link](/spec/abp/prefs.md#documentation-link) `%LINK%=reporter_privacy` in a new tab |
| [Design](https://eyeogmbh.invisionapp.com/share/JZSGUUTKBNU#/screens)[1] | `For all other issues, please contact us via support@adblockplus.org[2]`. |

[1] use info-icon.svg under [Assets](#assets)

[2] link email address to support@adblockplus.org

### Screenshot accessibility warning

The following text is only detectable by screen readers.

`To help us better understand the issue, a screenshot is required when submitting an issue report. Depending on the website you're currently on, the screenshot might contain personal information. If you're uncomfortable submitting a screenshot, please email your issue to (support@adblockplus.org)[1].`

[1] [support@adblockplus.org](mailto:support@adblockplus.org), subject: `[Issue Reporter]`

#### Cancel Button

Button labelled `Cancel`. Closes the [Issue Reporter Tab](#issue-reporter-tab) and the [Data gathering tab](#data-gathering-tab).

#### Continue Button

Button labelled `Continue`. Disabled as long as neither False Positive (#1.3) nor False Negative (#1.5) radio button is selected. Opens [Screenshot grabber](#screenshot-grabber).

[Back to top of page](#issue-reporter)

### Screenshot grabber

![](/res/abp/issue-reporter/screenshot.jpg)

The sreenshot will only capture what was visible on the user's screen when the issue reporter was launched.

| # | Text |
| --- | --- |
| 2.1 | `Highlight issue` |
| 2.2 | `Use the tools to highlight each ad or hide sensitive info on the screenshot. Highlighting ads will help us fix the issue faster.` |
| 2.3 | [`Highlight`](#highlight) |
| 2.4 | [`Hide`](#hide) |
| 2.5 | [`Cancel` button](#cancel-button) |
| 2.6 | `Continue` button opens [Issue Reporter Comment](#issue-reporter-comment) |

#### Highlight

![](/res/abp/issue-reporter/highlight.jpg)

The `Highlight` tool creates a rectangle outline on mouse drag anywhere within the screenshot.

`X` removes the rectangle. 

#### Hide

![](/res/abp/issue-reporter/hide.jpg)

The `Hide` tool creates a filled rectangle on mouse drag anywhere within the screenshot.

`X` removes the rectangle. 

### Issue Reporter Comment

![](/res/abp/issue-reporter/issue-reporter-comment.png)

| # | Text |
| --- | --- |
| 3.1 | `Leave a comment` |
| 3.2 | `It's helpful to enter a valid email address. This allows us to contact you to fix difficult issues like malware. It is not mandatory to enter an email address, and your address will never be shared.` |
| 3.3 | [`Email`](#email-imput) |
| 3.4 | `I would like to submit my issue anonymously` [annonymous checkbox](#anonymous-checkbox) |
| 3.5 | `Comment (optional):` |
| 3.6 | [Comment Textarea](#comment-textarea)
| 3.7 | [Share Checkbox](#share-checkbox)
| 3.8 | [Show report data link](#show-report-data-link)
| 3.9 | [`Cancel` button](#cancel-button) |
| 3.10 | [`Send report` button](#send-report-button) |

#### Email Input

Input of type [email](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/email) labelled `Email:`

Hightlighted red if invalid:
![](/res/abp/issue-reporter/issue-reporter-email-invalid.png)

#### Anonymous Checkbox

Checkbox labelled `I would like to submit my issue anonymously`.

If checked disables [Email Input](#email-input):
![](/res/abp/issue-reporter/issue-reporter-email-disabled.png)

#### Comment Textarea

Textarea labelled `Please help us better understand the issue by leaving a comment below.`.

### Share Checkbox

`Include information about my browser configuration, as well as a list of active extensions I'm using.`

The following data will be shared:
1. Configuration options (as opt-in)
    - Cookie behavior
    - JavaScript enabled?
    - Private browsing
	
1. Active extensions and Plug-Ins (as opt-in)
    - ID
    - Name
    - Type
    - Version

Selecting the checkbox will trigger a local browser dialog to request permission for the following items:
- contentSettings (for configuration options)
- management (for extensions)

Selecting the checkbox will follow the below flow:
1. User clicks checkbox
1. Browser shows dialog

- If user accepts permissions: Data gets added and checkbox gets checked
- If user rejects permissions: Checkbox stays unchecked

#### Show report data link

Link labelled `Click on this link to review the report data before sending`.

Is disabled until [Data gathering tab](#data-gathering-tab) has [loaded](https://developer.mozilla.org/en-US/docs/Web/Events/load) or the [Minimum data gethering time](#minimum-data-gethering-time) is elapsed.

Closes the [Data gathering tab](#data-gathering-tab) and opens the [Report Data Overlay](#report-data-overlay).

#### Send Report Button

Button labelled `Send report`.

Is disabled until [Data gathering tab](#data-gathering-tab) has [loaded](https://developer.mozilla.org/en-US/docs/Web/Events/load) or the [Minimum data gethering time](#minimum-data-gethering-time) is elapsed.

Closes the [Data gathering tab](#data-gathering-tab), starts the transfer of the [Report Data](#report-data) and opens [Send Report](#send-report).


[Back to top of page](#issue-reporter)

### Report Data Overlay

![](/res/abp/issue-reporter/issue-reporter-data.png)

1. [Report Data Textarea](#report-data-textarea)
1. [Report Data Close Button](#report-data-close-button)

#### Report Data Textarea

Readonly Textarea that shows the [report data](#report-data) to be sent.

#### Report Data Close Button

Button labelled `Cancel` closes the [Report Data Overlay](#report-data-overlay).

[Back to top of page](#issue-reporter)

### Send Report

![](/res/abp/issue-reporter/sending.jpg)

| # | Text |
| --- | --- |
| 4.1 | `Sending report` |
| 4.2 | `Please wait while Adblock Plus is submitting your report.` |
| 4.3 | [Close Button](#close-button) is disabled |

[Back to top of page](#issue-reporter)

### Report Sent

![](/res/abp/issue-reporter/report-sent.jpg)

| # | Text |
| --- | --- |
| 5.1 | `Report sent!` |
| 5.2 | `Thank you, your report has been successfully submitted. You can access it here:` |
| 5.3 | [Report Link](#report-link) |
| 5.4 | [Close Button](#close-button) |

#### Report Link

Link labeled with the *href* links to the submitted report.

### Close Button

Button labelled `Close` that will close the [Issue Reporter Tab](#issue-reporter-tab) and the [Data gathering tab](#data-gathering-tab).

### Data gathering tab

When the issue reporter is opened, the page for which an issue is being reported will be opened in the [Data gathering tab](#data-gathering-tab).

The [Data gathering tab](#data-gathering-tab) will gather all [Requests](#requests) and all [Filters](#filters) for as long as the tab is opened.

Once the page has loaded ([load](https://developer.mozilla.org/en-US/docs/Web/Events/load)) or the [minumum data gathering time](#minumum-data-gathering-time) has elapsed the [Data gethering tab](#data-gathering-tab) will scroll to the same viewport the user was using in the original page when he opnend the [Issue Reporter](#issue-reporter).

#### Minimum data gethering time

We'll gather [Requests](#requests) and all [Filters](#filters) for at least 5 seconds.

## Report Data


### Report

The root element of the report with the type of the report: *false positive* if the user selected the [False Positive Checkbox](#false-positive-checkbox) or *false negative* if the user selected the [False Negative Checkbox](#false-negative-checkbox).

```xml
<report type="false positive">
...
</report>
```

#### Main window

The main window the user opened the issue reporter on.

|Attribute|Value|
|---------|-----|
|*opener*|URL of the window that opened the main window. Omitted if main window wasn't opened by another window.|
|*referrer*|Referrer URL associated with the main window. Omitted if no referrer exists.|
|*url*|URL of the main window|

```xml
<window
    opener="https://www.example.com/"
    referrer="https://www.example.com/"
    url="https://www.heise.de/"/>
```

#### Subscriptions

A [subscription](#subscription) for each filter list the user has subscribed to.

```xml
<subscriptions>
...
</subscriptions>
```

##### Subscription

The subscription consisting of:

|Attribute|Value|
|---------|-----|
|*subscriptionVersion*|*version*|The version (YYYYMMDDhhmm) of the filter list|
|*lastDownloadAttempt*|Number of seconds between now and the last attempt download attempt|
|*lastDownloadSuccess*|Number of seconds between now and the last attempt download success|
|*softExpiration*|The soft expiration date of the filter list. (The softExpiration represents the number of seconds between the next potential download attempt and now. It may be postponed if user is offline.)|
|*hardExpiration*|The hard expiration date of the filter list. (The hardExpiration represents the number of seconds between the next potential download attempt in case download got postponed for too long and now.)|
|*downloadStatus*|The download status of a filter list|
|*disabledFilters*|The number of disabled filters in this filter list|

```xml
<subscription
    id="https://easylist-downloads.adblockplus.org/easylist.txt"
    version="201712110830"
    lastDownloadAttempt="-5871"
    lastDownloadSuccess="-5871"
    softExpiration="85997"
    hardExpiration="166929"
    downloadStatus="synchronize_ok"
    disabledFilters="0" />
```

#### AdblockPlus

Information about AdblockPlus:

|Attribute|Value|
|---------|-----|
|*version*|Version of the ABP Product|
|*locale*|Locale the user is using the ABP Product in|

```xml
<adblock-plus version="3.0.1" locale="en-US"/>
```

#### Application

Information about the Application (i.e. Browser) the user is using ABP in:

|Attribute|Value|
|---------|-----|
|*build*|Build identifier the Platform of the application (i.e. Browser) the user is using ABP in|
|*name*|Name of the Platform of the application (i.e. Browser) the user is using ABP in|
|*version*|Version of the application (i.e. Browser) the user is using ABP in|
|*vendor*|Name of the vendor of the application (i.e. Browser) the user is using ABP in|
|*userAgent*|UserAgent of the application (i.e. Browser) the user is using ABP in|

```xml
<application
    name="Firefox"
    version="57.0"
    vendor=""
    userAgent="Mozilla/5.0 (X11; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0"/>
```

#### Platform

Information about the Platform of the Application (i.e. Browser) the user is using ABP in:

|Attribute|Value|
|---------|-----|
|*name*|Name of the Platform of the application (i.e. Browser) the user is usign ABP in|
|*version*|Version the Platform of the application (i.e. Browser) the user is using ABP in|

```xml
<platform name="Gecko" version="54.0a2" build="20170509074737"/>
```

#### Requests

A [request](#request) for each request the main window made.

```xml
<requests>
...
</requests>
```

##### Request

|Attribute|Value|
|---------|-----|
|*location*|The location of a resource used by the page the issue report is issued for (Ommitted in case of type=ELEMHIDE).|
|*type*|The type of the request, see [here](https://hg.adblockplus.org/adblockpluscore/file/9ad72e008065/lib/filterClasses.js#l798) for a list of all supported types|
|*docDomain*|The domain of the document the resource is used on|
|*thirdParty*|Whether or not the resource is a third party resource (location doesn't match docDomain)|
|*count*|The number of times the request has been encountered|
|*filter*|The text of the filter in case a filter hit was recorded. Omitted if no filter applies to this request|

```xml
<request
    location="https://www.heise.de/js/mobile-deflect.min.js?ea85dfed7d6dc25326c5"
    type="SCRIPT"
    docDomain="www.heise.de"
    thirdParty="false"
    count="2" />
<request
    location="https://securepubads.g.doubleclick.net/gpt/pubads_impl_170.js"
    type="SCRIPT"
    docDomain="www.heise.de"
    thirdParty="true"
    count="1"
    filter="@@||g.doubleclick.net/gpt/pubads_impl_$script,domain=~kizi.com"/>
<request
    type="ELEMHIDE"
    docDomain="www.heise.de"
    thirdParty="false"
    count="1"
    filter="##.wingadblock"/>
```

#### Options

An [option](#option) for each user setting. Omitted if user hasn't opted-in to collecting this data.

```xml
<options>
...
</options>
```

##### Option

|Attribute|Value|
|---------|-----|
|*id*|Identifier of the option|

|Option ID|Option Value|
|---------|------------|
|*cookieBehavior*|To what extent cookies are allowed on the page.|
|*javascript*|Whether or not JavaScript is enabled for the page.|
|*privateBrowsing*|Whether or not the page is in a private tab|

|Cookie Behavior|Description|
|---------------|-----------|
|0|Cookies are allowed|
|2|Cookies are blocked|
|4|Cookies are allowed only for current session|

See also [network.cookie.cookieBehavior](https://developer.mozilla.org/en-US/docs/Mozilla/Cookies_Preferences#network.cookie.cookieBehavior).

```xml
<option id="javascript">true</option>
```

#### Extensions

An [extension](#extension) for each active extension. Omitted if user hasn't opted-in to collecting this data.

```xml
<extensions>
...
</extensions>
```

##### Extension

|Attribute|Value|
|---------|-----|
|*id*|Identifier of the extension|
|*name*|Name of the extension|
|*type*|Type of the extension|
|*version*|Version of the extension. Omitted in case of type=plugin.|

```xml
<extension
    id="{d10d0bf8-f5b5-c8b4-a8b2-2b9879e08c5d}"
    name="Adblock Plus"
    type="extension"
    version="2.9.1.4260-beta"/>
```

#### Filters

A [filter](#filter) for each filter hit in the main window.

```xml
<filters>
...
</filters>
```

##### Filter

|Attribute|Value|
|---------|-----|
|*text*|The text of an applied filter|
|*subscriptions*|Space separated list of subscriptionURLs|
|*hitCount*|The number of filter hits that where registered for this filter on the [Data gathering tab](#data-gathering-tab)|

```xml
<filter
    text="@@||g.doubleclick.net/gpt/pubads_impl_$script,domain=~kizi.com"
    subscriptions="https://easylist-downloads.adblockplus.org/exceptionrules.txt"
    hitCount="2"/>
<filter
    text="##.wingadblock"
    subscriptions="https://easylist-downloads.adblockplus.org/easylist.txt"
    hitCount="1"/>
```

#### Comment

The comment that was entered by the user. Omitted if no comment was entered.

```xml
<comment>
...
</comment>
```

#### Screenshot

The base64 encoded screenshot in png format.

|Attribute|Value|
|---------|-----|
|*edited*|true if [Highlight](#Highlight) or [Hide](#hide) tool was used, false otherwise|


```xml
<screenshot edited="true">data:image/png;base64,...</screenshot>
```

[Back to top of page](#issue-reporter)

## Assets

| Name | Asset | 
|-----------|---------------|
| abp-logo.svg | ![](/res/abp/issue-reporter/assets/abp-logo.svg) |
| info-icon.svg | ![](/res/abp/issue-reporter/assets/info-icon.svg) |

[Back to top of page](#issue-reporter)
