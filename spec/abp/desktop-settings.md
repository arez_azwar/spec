# Options Page
Options page for the ABP Browser extension.

The options page is used to change various options of the ABP browser extension.

1. [Navigation menu](#navigation-menu)
1. [About ](#about)
1. [General tab](#general-tab)
1. [Whitelist tab](#whitelist-tab)
1. [Advanced tab](#advanced-tab)
1. [Help tab](#help-tab)
1. [Assets](#assets)

## General requirements
### Bidirectionailty
Any change of state will be immediately reflected in the options page regardless
of where and how the change was made, so that the options page always reflects
the current state of the extension.

- If the user opens the option page more than once and makes changes to either of them, the change will be reflected in any of them and not just the one the change was made. 
- If a change to the state was made through other means e.g. auto update of filter list subscriptions, it will be reflected immediately in every instance of the options page.

### Style Guide
All UI patterns are defined in the [Options page Style Guide](/spec/abp/settings-style-guide.md).

### WCAG
Should implement the following [WCAG 2.0 Guidelines](http://www.w3.org/TR/2008/REC-WCAG20-20081211/):

- 2.1 Keyboard Accessible
- 2.4 Navigable

### Mouse behaviour
Anywhere an item is clickable the mouse cursor should follow the default browser behaviour.

## Navigation
Implement <https://www.w3.org/TR/2016/WD-wai-aria-practices-1.1-20161214/#tabpanel>

## Navigation menu
### Navigation menu default
![](/res/abp/desktop-settings/logo_Settings.png)

1. [Main title](#main-title)
1. [Navigation menu list](#navigation-menu-list)
1. [Contribute button](#contribute-button)
1. [About link](#about-link)

#### Main title
Logo [image](/res/abp/adblockplus-colour-web.svg)

`Settings`

This will appear consistently across all tabs.

All elements within the navigation menu are fixed according to the screen height.

#### Navigation menu list
Navigation names should match the headlines of each tab.
1. [General tab headline](#general-tab-headline)
1. [Whitelisted tab headline](#whitelisted-tab-headline)
1. [Advanced tab headline](#advanced-tab-headline)
1. [Help tab headline](#help-tab-headline)

#### Contribute button
Opens the [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=contribute` in a new tab

#### About link
Opens the [About](#about) overlay.

### About
![](/res/abp/desktop-settings/navigation-about.jpg)

#### Title
`About Adblock Plus`

#### Version number 
Version number of extension added dynamically. 

`Version number xxx`

#### Text
`Copyright © {current year} [eyeo GmbH]`[1]

`All rights reserved. Adblock Plus® is a registered  trademark of eyeo GmbH.`

`Privacy Policy`[2]

[1] eyeo GmbH - [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=imprint`

[2] [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=privacy`

#### Button to close layover
`CLOSE` 

X closes the layover.

## General tab
### General tab default
![](/res/abp/desktop-settings/general-tab.jpg)

1. [General tab headline](#general-tab-headline)
1. [General tab description](#general-tab-description)
1. [Privacy & Security section](#privacy-security-section)
1. [Acceptable Ads section](#acceptable-ads-section)
1. [Language section](#language-section)
1. [More filters section](#more-filters-section)
1. [Error state](#error-state)

#### General tab headline
`General`

Navigation label will match headline.

#### General tab description
`Determine what Adblock Plus shows and hides on websites`

### Privacy & Security section
![](/res/abp/desktop-settings/general-default-privacy-and-security.jpg)

1. [Privacy section headline](#privacy-tab-headline)
1. [Recommended filter lists](#recommended-filter-lists)
1. [Tooltip icon](#tooltip-icon)
1. [Tooltip pop-up](#tooltip-pop-up)

#### Privacy section headline
 `PRIVACY & SECURITY`

#### Recommended filter lists
Checkbox to install/uninstall each filter list.

| Filter list name | Filter list title | Tooltip | Filter list URL |
|-----------|---------------|---------------|--------------|
| Fanboy's Annoyances | `Block social media icon tracking` | `The social media icons on the websites you visit allow social media networks to build a profile of you based on your browsing habits - even when you don’t click on them. Hiding these icons can protect your profile.` | https://easylist-downloads.adblockplus.org/fanboy-annoyance.txt |
| EasyPrivacy | `Block additional tracking` | `Protect your privacy from known entities that may track your online activity across websites you visit.` | https://easylist-downloads.adblockplus.org/easyprivacy.txt |

#### Tooltip icon

Tooltip is triggered when users click on the `?` icon, clicking on `X` or outside of the pop-up closes the tooltip.

#### Tooltip pop-up

The tooltip will open in the direction that has the most vertical space available in the current viewport.

The tooltip pop-up should not extend beyond 12.5em in height. If the text overflows beyond this height, then add a scroll bar in.

Refer to the table in [Recommended filter lists](#recommended-filter-lists) for tooltip descriptions. 

### Acceptable Ads section
![](/res/abp/desktop-settings/general-default-acceptable-ads.jpg)

|Section|Content|Behavior|
|-------|-------|--------|
|1. Acceptable Ads section headline|`ACCEPTABLE ADS`| |
|2. Acceptable Ads section description|`Acceptable Ads are nonintrusive ads. They are the middle ground between ad blocking and supporting online content because they generate revenue for website owners.`| |
|3. Acceptable Ads options|`Allow Acceptable Ads` |Checked by default.|
|3a. Acceptable Ads options Description |`Acceptable Ads are not annoying and do not interfere with the content you are viewing. [Read more about the Acceptable Ads criteria][1]` |[1]: Opens [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=acceptable_ads_criteria` in a new tab|


#### Acceptable Ads opt-out Survey

**Design:** 
https://scene.zeplin.io/project/5bb73eebd31f1e2e9818f62a/screen/5cc33d52fca2322d1906b80a

*  When user opt-out from Acceptable Ads, tooltip will shown with the following text: `To help us improve Adblock Plus, mind sharing why you’ve turned off Acceptable Ads?`
*  `GO TO THE SURVEY` -  `%LINK%=acceptable_ads_survey` - Open in new tab
*  `NO, THANKS` - Dismiss message. Do NOT dismiss the message if user clicked somewhere else or moed to another Settings section. The message should be dismissed only by clicking on `NO, THANKS`

##### Only allow ads without third-party tracking
This appears as a subset of the `Acceptable Ads` options, which enables the Privacy-friendly Acceptable Ads filter list. 

This option is only active when `Allow Acceptable Ads` is selected, otherwise it will be inactive with reduced opacity.

###### Title:
`Only allow ads without third-party tracking`

###### Description:
`[Learn more][1]`

[1]: Opens [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=privacy_friendly_ads` in a new tab.

##### Do not track conditions
![](/res/abp/desktop-settings/general-default-acceptable-ads-dnt.jpg)

If a user selects `Only allow ads without third-party tracking` AND has DNT disabled, display the below text within the table:

`**Note:** You have **Do Not Track (DNT)** disabled in your browser settings. For this feature to work properly, please enable **DNT** in your browser preferences. [Find out how to enable DNT][1] and redirect to the official browser instructions for enabling DNT)`

[1]: Opens [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=(adblock_plus_chrome_dnt|adblock_plus_firefox_dnt|adblock_plus_opera_dnt|adblock_plus_edge_dnt)` depending on which browser the extension is running in a new tab.

#### Acceptable Ads notification

![](/res/abp/desktop-settings/general-default-acceptable-ads-message.jpg)

If a user selects `Block additional tracking` (EasyPrivacy) and `Allow Acceptable Ads`, then show the above notification within the page. This will continue showing until the user actively clicks to close the message. 

Clicking X or `OK, got it` closes the notification.

##### Notification text

```
We noticed you have both **Block additional tracking** and **Allow Acceptable Ad** enabled.

We want you to know that in order for advertisers to show you more relevant ads, there *may* be some tracking with Acceptable Ads.

If you prefer extra privacy, select the **Only allow Acceptable Ads that are privacy-friendly** checkbox below.

[OK, got it](closes notification)
```
### Language section
![](/res/abp/desktop-settings/general-default-language.jpg)

1. `LANGUAGE`
1. [Language section description](#language-section-description)
1. [Default language](#default-language)
1. [Change language](#change-language)
1. [Add language](#add-language)
1. `**TIP:** Only select the languages you need. Selecting more will slow down the ad blocker and, therefore, your browsing speed. `


#### Language section description
`Optimize Adblock Plus for the language(s) you typically browse websites in.

#### Default language

Language filter list selected based on the browser's default language. Bundled filter list labels are displayed as follows:

"*language of filter list* + English" [font color: ![#4A4A4A](https://placehold.it/15/4A4A4A/000000?text=+) `#4A4A4A`] "(*Filter list name*)" [font color: ![#BBB](https://placehold.it/15/BBB/000000?text=+) `#BBB`]

If it is an unbundled filter list, then hide `+ English`.

#### Change language
##### Label 
`CHANGE`

##### Behaviour
Button to trigger the [Language dropdown](#language-drop-down).

Clicking on a language within the dropdown will automatically close the window and *change the language filter subscription*. 

#### Add language
##### Label 
`+ ADD A LANGUAGE`
 
##### Behaviour
Button to trigger the [Language dropdown](#language-drop-down).

Clicking on a language within the dropdown will automatically close the window and *add the language filter subscription*. 

### Language drop down
![](/res/abp/desktop-settings/general-default-language-drop-down.jpg)

1. [Drop down](#drop-down)
1. [Selected language](#selected-language)
1. Hover state
1. `SELECT A LANGUAGE` 
1. [Scroll bar](#scroll-bar)

#### Drop down

The bundled language filter subscriptions are always used (i.e. it includes EasyList), unless it is EasyList, which is available unbundled. 

Display labels in the drop down as "*language of filter list* + English", unless it is EasyList, then only display `English`.

Clicking anywhere outside of the drop down, or on a [selected language](#selected-language) closes the drop down.

#### Selected language

Already added filter subscriptions will appear greyed out and disabled in the drop down.

#### Scroll bar

The size of the layover menu should correspond to the screen size. The scroll bar should adjust accordingly. 

#### Multiple languages
![](/res/abp/desktop-settings/general-default-language-multiple.jpg)

##### Behaviour
- When there is more than one language filter list in the table, the  [Change language](#change-language) element turns into a bin icon. All filter lists are removable.
- When there is only one language filter list in the table, the remaining filter list is not removable, and the user is only allowed to  [Change language](#change-language) the filter list - this triggers the [Language modal window](#language-drop-down).

##### Removing a language
To remove a language from the table, click the bin icon in the language row. 

#### Language filter list - empty state
![](/res/abp/desktop-settings/general-tab-language-empty.jpg)

##### Text
`You don't have any language-specific filters.`

##### Conditions
When all language filter lists have been removed from the extension.

Note: it is only possible to remove all language filter lists in [Remove filter list subscriptions](#remove-filter-list-subscriptions) in the Advanced tab.

### More filters section

![](/res/abp/desktop-settings/general-default-more-filters.jpg)

1. [More filters section headline](#language-section-headline)
1. [More filters section description](#language-section-description)
1. [More filter subscriptions](#more-filter-subscriptions)
1. [More filters tooltip](#more-filters-tooltip)
1. [Remove more filters](#remove-more-filters)
1. [More filters note](#more-filters-note)

#### More filters section headline
`More filters`

#### Language section description
`These are additional filters you previously added to Adblock Plus.`

#### More filter subscriptions
All filter lists that a user subscribes to which are not one of Adblock Plus's recommended filter lists (i.e. not recommended within the `Privacy & Security` and `Language` sections) will be shown here. 

#### Remove more filters
You can remove filters by clicking on the `Remove` button.

#### More filters notes

Description `**Note:** You should only use third party filter lists from authors that you trust.`

### Error state

When there is a failure of an action within the page, i.e. if a filter list download fails, display the following error message `Uh oh! Something went wrong. Please try again.`.

This will appear as a banner that will animate from the top down for 3 seconds.

![](/res/abp/desktop-settings/general-tab-error.jpg)

## Whitelist tab
[Back to top of page](#options-page)

### Whitelist tab empty
![](/res/abp/desktop-settings/whitelisted-websites.jpg)

1. [Whitelist tab headline](#whitelist-tab-headline)
1. [Whitelist tab description](#whitelist-tab-description)
1. [Whitelist textinput](#whitelist-textinput)
1. [Add whitelisted domain button](#add-whitelisted-domain-button)
1. [Add whitelisted domain error message](#add-whitelisted-domain-error-message)
1. [Empty Whitelist placeholder](#empty-whitelist-placeholder)

#### Whitelist tab headline
`Whitelisted websites`

Navigation label will match headline.

#### Whitelist tab description
`You’ve turned off ad blocking on these websites and, therefore, will see ads on them. [Learn more][1]`

[1]: [Documentation link](/spec/abp/prefs.md#documentation-link) *whitelist*

#### Whitelisting a website
##### Behaviour
When a duplicate entry is made, move the original entry to the top of the list and discard the duplicate.

Convert URLs into domains when adding domain to whitelist.

##### Text input 
Text input to enter domains to be added to the whitelist.

##### Label in text input 
`e.g. www.example.com`

#### Add whitelisted domain button
##### Overview
Button to submit [Whitelist text input](#text-input).

If clicked whitelist domain and show [Whitelisted domain added notification](#whitelisted-domain-added-notification).

Refer to [w3 guidelines for keyboard interaction](https://www.w3.org/TR/2016/WD-wai-aria-practices-1.1-20161214/#button)

> A whitelisted domain is an exception rule filter for a certain host.

##### Button label 
`Add website`

#### Add whitelisted domain error message
Shows an error message if [Add whitelisted domain text input](#text-input) doesn't validate.

#### Empty Whitelist placeholder
Placeholder text shown as long as there are no whitelisted domains.

`You don't have any whitelisted websites.`

`Websites you trust and want to allow ads on will be shown here.`

### Whitelist tab populated
![](/res/abp/desktop-settings/whitelisted-websites-populated.jpg)

#### List of whitelisted domains
##### Overview
Displays all whitelisted domains. 

##### Behaviour
List items are sorted alphabetically when loaded.

Move newly added items (from the current session) to the top of the list.

When a duplicate domain entry is made, move the original entry to the top of the list.

<!-- This is to technical, find a better way to describe it -->
***Note***: *All whitelisted domains* does only refer to exception rules that match the following regexp and a member of the *SpecialSubscription* filter list:

```javascript
/^@@\|\|([^\/:]+)\^\$document$/
```

#### Remove whitelisted domain link

Clicking on the bin icon deletes the corresponding domain from the whitelist.

### Whitelisted domain added notification
![](/res/abp/desktop-settings/whitelisted-websites-notification.jpg)

#### Whitelisted domain added notification
##### Overview
The notification should appear as an overlay sliding in from the top of the page.

The notification will disappear if the X is clicked or automatically after 3 seconds.

##### Notification message
`Website has been whitelisted.`

#### Whitelisted Webpage URL from Bubble UI
- When specific webpage URL was whitelisted from Bubble UI, show the entire address without http(s) and www.
- If the URL is too long, truncate with `...` at the end

## Advanced tab
[Back to top of page](#options-page)

### Advanced tab: Default
Design: https://eyeogmbh.invisionapp.com/share/3GS76ARU5D6

1. [Advanced tab headline](#advanced-tab-headline)
1. [Advanced tab description](#advanced-tab-description)
1. [Customize section](#customize-section)
1. [Filter lists section](#filter-lists-section)
1. [Advanced tab: Custom filter](#advanced-tab-custom-filter)

#### Advanced tab: Headline
`Advanced`

Navigation label will match headline.

#### Advanced tab: Description
`Customize Adblock Plus, add or remove filter lists, create and maintain your own filter lists`

#### Customize section header

`CUSTOMIZATIONS`

#### Customize section options

| Behaviour | Text | Tooltip | 
|---------------|---------------|---------------|
| Checkbox to en-/disable icon number | `Show number of ads blocked in icon` | n/a |
| Checkbox to en-/disable [Block Element Context Menu Entry](#TBA) | `Show 'Block Element’ right-click menu item` | `Temporarily block annoying items on a webpage, e.g. images or animated slideshows.` |
| Checkbox to en-/disable [Adblock Plus Developer Tools Panel](#TBA) | `Show 'Adblock Plus' panel in developer tools` | `View blocked and whitelisted items from your browser's developer tools panel.` |
| Checkbox to en-/disable [Notifications](#TBA) | `Show useful notifications` | `Allow notifications from Adblock Plus (notifications related to critical performance issues will always be shown).` |

Tooltip behaviour is specified in [Tooltip icon](#tooltip-icon).

#### Filter lists section
Filter list subscriptions are displayed in a grid format. Keyboard interactions should follow guidelines as specified by [w3 data: Grids for presenting tabular information](https://www.w3.org/TR/2016/WD-wai-aria-practices-1.1-20161214/#dataGrid)

1. [Filter list section header](#filter-list-section-header)
1. [Filter list section description](#filter-list-section-description)
1. [Filter list subscriptions status](#filter-list-subscriptions-status)
1. [Filter list title](#filter-list-title)
1. [Filter list subscriptions updates](#filter-list-subscriptions-updates)
1. [Filter list subscriptions settings](#filter-list-subscriptions-settings)
1. [Remove filter list subscriptions](#remove-filter-list-subscriptions)
1. [Filter list buttons](#filter-list-buttons)

#### Filter list section header
`FILTER LISTS`

#### Filter list section description
`Each Adblock Plus setting functions because of a filter list. Below are the filter lists that correspond to all of your Adblock Plus
settings. You can also add additional filters created and maintained by our trusted community. [Learn more][1]`

[1]: [Documentation link](/spec/abp/prefs.md#documentation-link) *subscriptions*

#### Filter list subscriptions status
##### Column title
`Status`

##### Toggle 
Indicates whether or not the filter list subscription is active.

*Exception:* The toggle is hidden for The Acceptable Ads and privacy-friendly Acceptable Ads filter lists.

#### Filter list subscriptions
##### Overview
Displays name of the filter list
 
#### Filter list error
##### Filter list error message
When there is a a problem with the filter list, the text in the `Last updated` column changes to the display the relevant error message. 

See [Filter list error statuses](#filter-list-error-statuses).

##### Filter list error popup
Upon clicking on the error status a popup box appears to display the entire text. 

Clicking anywhere outside of the box, or on the 'X' closes the popup.

##### Filter list error statuses

   | Error message | Trigger | Settings status | Settings behaviour |
   |-----------|---------------|--------------|------------|
   | `Not a valid address. Check the URL.` | Invalid URL provided | no change | n/a |
  | `Download failed. URL must start with https://.` | URL is not HTTPS | no change | n/a |
   | `Download failure` | No HTTP Response or Response is anything other than 2XX | 'settings.svg' icon changes to 'reload.svg' icon | Clicking the 'reload.svg' icon updates the filter list |
   | `Not a valid filter list` | The filter list is invalid | no change | n/a |
   | `Checksum mismatch. Try (downloading)[1] the file again.` | Checksum of filter list and checksum in filter list header doesn't match | 'settings.svg' icon changes to 'reload.svg' icon | Clicking the 'reload.svg' icon updates the filter list |

[1] Triggers a filter list update 

##### Behaviour
Lists are sorted alphabetically when loaded, except for "Allow nonintrusive advertising" and "Allow nonintrusive advertising without third-party tracking", which should always appear at the bottom of the list.

Move newly added items (from the current session) to the top of the list.

#### Filter list subscriptions updates
##### Column title
`Last updated`

##### Date/Time of last filter list update
  - Show `Just now` if less than 5 minutes ago
  - Show `minutes ago` if between 5 to 60 minutes ago
  - Show `hours ago` if the last update was between 60 minutes to 24 hours ago
  - Localize dates according to the [Intl.DateTimeFormat](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/DateTimeFormat Show)
  - Show `Updating` while the list is getting updated
  - Show `Download fail` when there is a problem downloading the filter list
  - All date and time status for filter lists should be updated as filter lists are updated.

#### Filter list subscriptions settings
A Gear Icon opens the [Filter list menu](#filter-list-menu)

#### Remove filter list subscriptions
trash.svg icon deletes the filter list. 

#### Filter list buttons 

|Button|Functionality|
|-----------|-------------|
|`+ Add built-in filter lists`|Opens [Add filter list drop down](#add-built-in-filter-list-dropdown).|
|`Update all filter lists`|Clicking updates all dates and time statuses for filter lists.|
|`+ Add filter list via URL`|Clicking opens the [Add filter list via URL popover](#add-filter-list-via-url)|

## Filter list menu
##### Overview
Menu to update and get access to more information on a filter list subscription.

The menu should follow [aria best practices](https://www.w3.org/TR/wai-aria-practices-1.1/#menu) for keyboard interactions.  

##### Behaviour
Clicking outside the menu closes itn.

Clicking on a menu item closes the menu.

1. [Update filter list link](#update-filter-list-link)
1. [Filter list homepage link](#filter-list-homepage-link)
1. [Filter list source link](#filter-list-source-link)

| # | Text | Behaviour | 
|---------------|---------------|---------------|
| 1 | `Update now` | Updates the filter list |
| 2 | `Website` | Link to the homepage of the filter list | 
| 3 | `Source` | Link to the filter list file | 

[1] [Additional subscriptions](/spec/abp/prefs.md#additional-subscriptions) cannot be removed and the [Remove filter list link](#remove-filter-list-link) should be disabled in this case.

## Add built-in filter list dropdown

#### Overview

- All filter lists are displayed as follows: "*Filter list title*  (*Filter list description*)"
- [Non-language filter lists](#non-language-filter-lists) are displayed on top, while  [Language filter lists](/spec/abp/filter-lists.md) are listed below the `LANGUAGE FILTER LISTS` title 
- Installed filter lists are noninteractive
- Installed and enabled filter lists are marked with a check and have light blue background
- Installed and disabled filter lists have light grey background
- Clicking on a not installed filter list closes the dropdown and adds the enabled filter list in the Filter Lists table
- Clicking the `ADD BUILD-IN FILTER LISTS` while the dropdown in opened, or clicking anywhere outside of the dropdown, closes the dropdown

### Non-language filter lists

| Filter list title | Description | Subscribe link | Homepage |
|----|----|----|---|
| ABP filters | ABP Anti-Circumvention Filter List | https://easylist-downloads.adblockplus.org/abp-filters-anti-cv.txt | https://github.com/abp-filters/abp-filters-anti-cv |
| EasyPrivacy | Block additional tracking | https://easylist-downloads.adblockplus.org/easyprivacy.txt | https://easylist.to/ |
| Fanboy's Social Blocking List | Block social media icons tracking | https://easylist-downloads.adblockplus.org/fanboy-social.txt | https://easylist.to/ |

## Add filter list via URL

| Element | Content | 
|---------------|---------------|
| Title | `ADD FILTER LIST VIA URL` |
| Warning | `URL must start with https://` |
| URL input | `Filter list URL` |
| Button | `ADD A FILTER LIST` |
| Button | `CANCEL` |

##### Overview

- Input field validates as is typed against:
<!-- This is to technical, find a better way to describe it -->
```javascript
/^https?:/i
```
- As soon as the input is validated, the warning disappears
- Clicking `ADD A FILTER LIST` when the input field contains valid URL, adds the filter list to the Filter Lists table
- Clicking `ADD A FILTER LIST` when the input field contains invalid URL briefly flashes the warning message, while keeping the popover opened
- Clicking `CANCEL` closes the popover without discarding the content of the input field
- Tab focus on the `ADD A FILTER LIST` button before `CANCEL`

### Add filter subscription dialogue

##### Overview

| Element | Content | 
|---------------|---------------|
| Dialog title | `Are you sure you want to add this filter list?` |
| Warning | `Enter a valid URL` |
| Title | `<Filter list title>` (if a title exists, otherwise hide) |
| URL | `<Filter list URL>` |
| Button | `Yes, use this filter list` |

- Dialog is opened when a user adds a filter list by clicking on a subscribe link from an external source.
- Clicking `Yes, use this filter list` button adds the filter list. 
- Clicking on the `X` in the upper right corner closes the dialog without adding the filter list.

#### Filter list table - empty state

##### Overview 
When all filter lists have been removed from the extension.

##### Text
`You have not added any filter lists to Adblock Plus. Filter lists you add will be shown here.`

### Advanced tab: Custom filter
Design: https://eyeogmbh.invisionapp.com/share/XGT31Y9H3DP#/screens

1. `YOUR CUSTOM FILTERS`
1. `Create and maintain your own filters to further control what content Adblock Plus allows or blocks. [Learn how to write filters (English only)](%LINK%=filterdoc)`
1. `My filter list`
1. [Input field](#input-field)
1. [Add button](#add-button)
1. [Checkbox](#checkbox)
1. [Filter rule toggle](#filter-rule-toggle)
1. [Filter rule](#filter-rule)
1. [Alert](#alert)

#### Input field

Instructional prompt in the text box reads `Search or add filter(s) (e.g. /ads/track/*) `.

##### Searching and adding filters:
After at least 3 characters are entered, the table automatically starts selecting filter rules and scrolling to the first item in the filter list that matches the entered pattern. This check is performed after each keystroke.

 - If an exact match is found, the [+ ADD button](#add-button) is disabled, to prevent adding a duplicate.
 - If no matches, or multiple matches are found, the [+ ADD button](#add-button) is active and the unique filter rule can be added to the table via pressing [+ ADD button](#add-button) or `enter key`. After adding a filter, the table scrolls to the top and new filter is added as a new row to the top.

The input field supports adding multiple filters. Multiple filters are automatically added by pasting multiline filters directly into the input field. After pasting, the input field remains empty, table scrolls to the top and filters are added to the top of the table. 

###### Exceptions for adding a single filter:
 - If an invalid filter of type error, as defined in the [Alert](#alert) section, is to be added to the table, its corresponding error is shown in the table's footer, filter is not added, and entered filter pattern remains in the input field.
 - If a filter list header is to be added, it gets silently ignored. There are no errors shown and the input field is cleared after clicking the [+ ADD button](#add-button) or `enter key`.

###### Exceptions for adding multiple filters:
 - If pasted filters contain a duplicated filter, the existing filter is replaced by the pasted one.
 - If pasted filters contain a filter list header, it is silently ignored and only valid filters get added to the table.
 - If pasted filters contain an invalid filter of type error, as defined in the [Alert](#alert) section, its corresponding error, together with the line number is shown in the table's footer. None of the pasted filters get added to the table and the input field contains all pasted filters in one single line.

#### Add button 

The `+ADD` button is disabled if the input field is empty, or the pattern entered matches exactly one filter in the filter list. Clicking `+ADD` button scrolls the table to the top an adds the filter rule as a new row at the top of the list.

#### Checkbox

Checkbox for selecting filters. Checking the checkbox in the column header selects, while unchecking it deselects all filters in the filter list. Column header changes to a darker shade on hover to indicate that it is interactive. 

Checking/unchecking the checkbox next to an individual filter will select/deselect that filter. Selected filter's row in the table changes to a darker shade to indicate that the filter is selected.

When at least one filter is selected, the `Delete` and `Copy selected` buttons are shown to delete and copy the selected filters respectively, as shown below. When no filters are selected the buttons are hidden. After copying selected filters, the filters remain selected and the buttons visible. After deleting selected filters, no filters are selected and the buttons are hidden.

![](/res/abp/desktop-settings/custom-filter-multiple-selected.jpg)

#### Filter rule toggle

Toggle to enable/disable individual filters. Toggle is hidden for comments.

Clicking on the column header sorts the filter list so that enabled filters are on top of the table, while disabled filters are on the bottom. Clicking it again reverses the order. Column header changes to a darker shade on hover to indicate that it is interactive.

#### Filter rule

Sortable list of filter rules. Clicking on the column header sorts the list alphabetically, clicking the header again reverses the order. Column header changes to a darker shade on hover to indicate that it is interactive. Within a session, filters should be ordered with the last added filters at the top of the table. 

##### Editing filters:

A filter can be edited by clicking in its Filter rule field. Changes are saved by pressing `enter key` or clicking outside of the filter field. If the edited filter is valid after saving, the Filter rule field briefly flashes green. Edited filters stay where they are in the table and are not moved to the top. 

###### Special editing cases:
 - Editing a filter into a comment hides the [Filter rule toggle](#filter-rule-toggle) and vice versa.
 - Editing a filter into a filter of type warning, as defined in the [Alert](#alert) section, makes a grey alert icon appear in the Alert column and vice versa. Hovering on the icon shows filter's corresponding warning message.
 - Editing a filter into an invalid filter of type error, as defined in the [Alert](#alert) section, makes a red alert icon appear in the Alert column. Hovering on the icon shows invalid filter's corresponding error message. Changes are not persistent and the filter is reverted back to its valid state on refresh.

#### Alert

A grey warning icon is displayed next to a filter of type warning. A tooptip on hover is shown, containing the message as defined in the table below. Such filters are still considered valid, and can therefore be added to the filter table. 

A red warning icon is displayed next to a filter of type error. A tooptip on hover is shown, containing the message as defined in the table below. Such filters are considered invalid and can not be added to the filter table, but an existing filter can be edited into an invalid state.

Clicking on the column header sorts the filter list so that filters with grey warning icon are shown on top of the table, while all other filters are on the bottom. Clicking it again reverses that order. Column header changes to a darker shade on hover to indicate that it is interactive.


| Error states | Messages | Type |
| --- | --- | --- |
| elemhideemulation nodomain | `No active domain specified for extended element hiding filter.` | error |
| invalid csp | `Invalid Content Security Policy.` | error |
| invalid domain | `Invalid (or empty) domain specified.` | error |
| invalid regexp | `Invalid regular expression.` | error |
| snippet nodomain | `No active domain specified for snippet filter.` | error |
| unknown option | `Unknown filter option.` | error |
| unexpected filter list header | `Filter list headers aren't allowed here.` | error |
| short pattern | `To ensure a fast filter, please check the length of the pattern and ensure it doesn't contain a regular expression.` | warning |
| duplicate filter | `Something went wrong. Please try again.` | error |
| filter action failed | `Something went wrong. Please try again.` | error |

#### Empty state
Design: https://eyeogmbh.invisionapp.com/share/XET31YGWDUN#/screens

Empty state is shown when the custom filters table is empty.

Instructional prompt in the text box reads `Search or add filter(s) (e.g. /ads/track/*)`.

#### Store Rating

This feature should be enabled for ABP extension (both stable  and development) versions for the following browsers: Chrome, Firefox and Opera only

Design: [Invision Link](https://eyeogmbh.invisionapp.com/share/DBSVQXGAXY5#/screens/372815512)

Waving icon: [Link](res/abp/desktop-settings/assets/waving.svg)
Star icon: [Link](res/abp/desktop-setting/assets/rating_star.svg)
Heart icon: [Link](res/abp/desktop-setting/assets/heart.svg)

On the right area should be a heart icon, clicking on it should open a message that ask the user to rank us in the stores.
- Click on the x should close the message.
- When User mouse over the stars - they should be lighted. So if user mouse over the 4th star (from the left), 4 stars should be highlighted.
  - For RTL languages - highlight the stars from right to left.
- If user pick stars 4-5 send him to the relevant store, directly to the review page if possible. 
  - CWS URL should include parameter "ref=store-rating" so we can see it in Google Analytics
- If user pick 3 stars or less, send him to the 3 stars redirect link. 
- Keep showing this feature, even if user clicked on the stars


| Text position | Content |
|----------------|----------------|
| Title | `Enjoying Adblock Plus?` |
| Description | `Please take a moment and help spread the word by rating Adblock Plus. Thanks for your support!` |

| Store | Link |
|-------|------|
|Chrome|`%LINK%=chrome_review`|
|Firefox|`%LINK%=firefox_review`|
|Opera|`%LINK%=opera_review`|
|3 Stars Chrome|`%LINK%=chrome_review_low`|
|3 Stars Firefox|`%LINK%=firefox_review_low`|
|3 Stars Opera|`%LINK%=opera_review_low`|

## Help tab
[Back to top of page](#options-page)

![](/res/abp/desktop-settings/help-tab.jpg)

1. [Help tab headline](#help-tab-headline)
1. [Help tab description](#help-tab-description)
1. [Support section](#support-section)
1. [Get in touch section](#get-in-touch-section)

#### Help tab: Headline
`Help`

Navigation label will match headline.

#### Help tab: Description
`Find help or get in touch with us`

#### Support section
##### Title 
`Support`

##### Bullet list (note: links are specific to language settings)
- `Looking for answers to your questions? [Visit our Help Center (English only)]`(https://adblockplus.org/redirect?link=help_center_abp_en)
- `Found a bug?`  `Send us a bug report` (opens [Documentation link](/spec/abp/prefs.md#documentation-link) `%LINK%=adblock_plus_report_bug` in a new tab)
- `Want support from our community?`  `Go to the Forum`  (depending on the application open the application specific [Documentation link](/spec/abp/prefs.md#documentation-link)  `%LINK%=firefox_support` `%LINK%=chrome_support` `%LINK%=opera_support` or `%LINK%=edge_support`in a new tab)
- `Email: support@adblockplus.org`

#### Get in touch section
##### Title 
`Get in touch`

##### Description 
`Have a question or a new idea? We're here to help.`

##### Social media icons and links
| Social Media | Link URL | Browser locale |
|----------------|----------------| ----------------|
| `Twitter` | Opens [Documentation link](/spec/abp/prefs.md#documentation-link) `%LINK%=social_twitter` in a new tab | For all locales except *zh* |
| `Facebook` | Opens [Documentation link](/spec/abp/prefs.md#documentation-link) `%LINK%=social_facebook` in a new tab | For all locales except *zh* |
| `Weibo` | Opens [Documentation link](/spec/abp/prefs.md#documentation-link) `%LINK%=social_weibo` in a new tab | Only for the locales *zh* |

## Assets
[Back to top of page](#options-page)

| Name | Asset | 
|-----------|---------------|
| ABP_Logo_Outline.svg | ![](/res/abp/desktop-settings/assets/ABP_Logo_Outline.svg) |
| attention.svg | ![](/res/abp/desktop-settings/assets/attention.svg) |
| box-circle.svg | ![](/res/abp/desktop-settings/assets/box-circle.svg) |
| checkbox-empty.svg | ![](/res/abp/desktop-settings/assets/checkbox-empty.svg) |
| checkbox-yes.svg | ![](/res/abp/desktop-settings/assets/checkbox-yes.svg) |
| checkmark.svg | ![](/res/abp/desktop-settings/assets/checkmark.svg) |
| code.svg | ![](/res/abp/desktop-settings/assets/code.svg) |
| copy-content.png | ![](/res/abp/desktop-settings/assets/copy-content.png) ![](/res/abp/desktop-settings/assets/copy-content@2x.png) |
| delete.svg | ![](/res/abp/desktop-settings/assets/delete.svg) |
| email.svg | ![](/res/abp/desktop-settings/assets/email.svg) |
| facebook.svg | ![](/res/abp/desktop-settings/assets/facebook.svg) |
| globe.svg | ![](/res/abp/desktop-settings/assets/globe.svg) |
| googleplus.svg | ![](/res/abp/desktop-settings/assets/googleplus.svg) |
| reload.svg | ![](/res/abp/desktop-settings/assets/reload.svg) |
| settings.svg | ![](/res/abp/desktop-settings/assets/settings.svg) |
| snail.svg | ![](/res/abp/desktop-settings/assets/snail.svg) |
| tooltip.svg | ![](/res/abp/desktop-settings/assets/tooltip.svg) |
| tooltip-arrow.svg | ![](/res/abp/desktop-settings/assets/tooltip-arrow.svg) |
| trash.svg | ![](/res/abp/desktop-settings/assets/trash.svg) |
| twitter.svg | ![](/res/abp/desktop-settings/assets/twitter.svg) |
| toggle-enabled.svg | ![](/res/abp/desktop-settings/assets/toggle-enabled.svg) |
| toggle-disabled.svg | ![](/res/abp/desktop-settings/assets/toggle-disabled.svg) |
| weibo.svg | ![](/res/abp/desktop-settings/assets/weibo.svg) | 
