# Adblock Plus: Bubble UI

The bubble UI is the menu which is triggered when the Adblock Plus icon in the browser toolbar is clicked. The bubble UI displays notifications, top-level information on the extension usage and provides access to the extension's settings.

1. [General requirements](#general-requirements) 
1. [Index](#index) 
1. [Toolbar icon](#toolbar-icon)
1. [Notifications](#notifications)
1. [Style guide](#style-guide) 
1. [Assets](#assets)

## General requirements

- Width: 340px

## Index

![](/res/abp/popup/index-menu.png)
 
1. Logo [image](/res/abp/adblockplus-colour-web.svg)
1. Gear icon - Launches [Settings page](/spec/abp/desktop-settings.md) in a new tab (Refer to the [Style guide](#style-guide) for hover/click states)
1. [Toggle](#toggle)
1. `Number of items blocked`
1. [Counter panel](#counter-panel)
1. [Report issue](#report-issue) 
1. [Block element](#block-element)
1. `Want ad blocking on your mobile device? [1] [2]` 

[1] adblock_browser_ios_store 

[2] adblock_browser_android_store

## Toggle

![](/res/abp/bubble-ui/ABP-Enabled-Disabled.png) 

The toggles to enable/disable the extension are only active on HTTP(S) sites, otherwise they are [inactive](#inactive) (i.e. an extension page).

### Enable/Disable behavior:

- Disabling the toggle on domain level also disables the toggle on page level.
- When the toggle on domain level is disabled, the page level toggle can not be enabled.
- Enabling the toggle on domain level also enables the toggle on page level.

### Enable/Disable text:
`Block ads on:` 

`This website:`
`<exampledomain.com>`

`This page:` 
`</examplepath>`

- Long domains are truncated with '...' at the end
- Only URL's path is shown for page level

### Refresh

**Enable/Disable Design:** 
https://eyeogmbh.invisionapp.com/share/TYSWLRTQ37D#/

- After enabling or disabling any of the toggles, the page needs to be reloaded for the setting to take effect.
- Refresh message is shown when toggles' state changes
  - Title: `Refresh this page`
  - Description: `Click the button below for changes to take effect.`
  - Button: `Refresh`
- Clicking `Refresh` reloads the webpage and closes the bubble UI.
- If changes to toggles are reverted before the page is reloaded, the refresh message disappears

# Inactive

![](/res/abp/popup/inactive.png) 
 
The extension is inactive on non HTTP(S) sites.

Both the toggle panel and ad counter for "this page" are hidden.

## Counter panel

 ![](/res/abp/popup/blocked-items.png) 

Displays the total number of blocked requests:
- `<#> on this page`: `#` shows the number of blocked requests, when the extension is active on the current page and session. When the extension is inactive on the active page, the panel is hidden. 
- `<#> in total` : `#` shows the total number of blocked requests by the extension since it had been installed. 

## Report issue 

This is only active when the extension is active on the page. Clicking this launches the [Issue Reporter](/spec/abp/issue-reporter.md) in a new tab. 

For translations that run long, then stack the items for [report issue](#report-issue) and [block element](#block-element) into a single column.

Refer to the [Style guide](#style-guide) for hover/click states.

## Block element

`Block element` only appears on webpages when the extension is active and when there are detectable elements, otherwise this feature is inactive. 

Refer to the [Style guide](#style-guide) for hover/click states.

Clicking `Block element` displays the below menu and opens the [filter composer window](/spec/abp/composer.md) in the browser.

![](/res/abp/popup/block-element.png)

  ```
  # Block element
  
  Click an element on the page to block it.
  ```

Click `Cancel` to close the menu and return to the main bubble UI menu.
 
This menu will stay displayed as long as long as the element selector window is open. 

## Notifications 

| Informational notification | Critical notification |
| ---- | ---- |
| ![](/res/abp/popup/info-notification.png) | ![](/res/abp/popup/critical-notification.png) |

### All notifications (except critical notifications)

1. Clicking 'Close' hides the current message.
1. Clicking 'Stop showing notifications' hides the current notification, prevents further notifications from showing up (except critical ones), and unchecks the "Show useful notifications" option.

### Critical notification

Clicking `Close` hides the current message.

## Toolbar icon

| Icon state | Description |
| --- | --- |
| ![](/res/abp/popup/toolbar-icon.png) | Extension enabled |
| ![](/res/abp/popup/abp-whitelisted.png) | Extension disabled |
| ![](/res/abp/popup/toolbar-icon-count.png) | Number of elements blocked on the active tab |
| ![](/res/abp/popup/abp-notification-critical.png) | Critical alert notification |
| ![](/res/abp/popup/abp-notification-information.png) | Informational notification |

## Style guide

All clickable elements (i.e. gear icon, report issue, block element, cancel) should have the following states: 
1. **Active:** Dark grey
2. **Hover / On click / Selected:** Highlighted in blue
3. **Inactive:** (not always applicable) Light grey

### Report issue states

| State | Visual states |
| --- | --- |
| Active | ![](/res/abp/popup/report-issue-active.png) |
| Hover / On click / Selected | ![](/res/abp/popup/report-issue-selected.png) |

### Block element states

| State | Visual states |
| --- | --- |
| Active | ![](/res/abp/popup/block-element-active.png) |
| Hover / On click / Selected | ![](/res/abp/popup/block-element-selected.png) |
| Inactive | ![](/res/abp/popup/block-element-inactive.png) |


## Assets 

| Name | File |
| --- | --- |
| iconBlockElement.svg | ![](/res/abp/popup/assets/iconBlockElement.svg) |
| iconBlockElementHover.svg | ![](/res/abp/popup/assets/iconBlockElementHover.svg) |
| iconClose.svg | ![](/res/abp/popup/assets/iconClose.svg) |
| iconOff.svg | ![](/res/abp/popup/assets/iconOff.svg) |
| iconOn.svg | ![](/res/abp/popup/assets/iconOn.svg) |
| iconReportIssue.svg | ![](/res/abp/popup/assets/iconReportIssue.svg) |
| iconReportIssueHover.svg | ![](/res/abp/popup/assets/iconReportIssueHover.svg) |
| options.svg | ![](/res/abp/popup/assets/options.svg) |
| iconAndroid.svg | ![](/res/abp/popup/assets/iconAndroid.svg) |
| iconApple.svg | ![](/res/abp/popup/assets/iconApple.svg) |
| iconInfo.svg | ![](/res/abp/popup/assets/iconInfo.svg) |
| iconCritical.svg | ![](/res/abp/popup/assets/iconCritical.svg) |





